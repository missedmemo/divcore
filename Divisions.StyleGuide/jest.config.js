// defaults to browser only (testEnvironment: 'jest-environment-jsdom')

module.exports = {
  preset: "ts-jest",

  /*
  moduleNameMapper: {
    "\\.(sa|sc|c)ss$": "identity-obj-proxy",
    "\\.(png|jpe?g|gif|svg)$": "identity-obj-proxy",
  },
  */

  setupFilesAfterEnv: ["@testing-library/jest-dom/extend-expect"],
};
