import React, { useState } from "react";

export interface IToggleButton {
  toggled?: boolean;
  txtNotToggled?: string;
  txtToggled?: string;
}

const ToggleButton = ({
  toggled = false,
  txtNotToggled = "Start",
  txtToggled = "Stop",
}: IToggleButton): JSX.Element => {
  const [isToggled, setIsToggled] = useState(toggled);

  const flip = () => setIsToggled(!isToggled);

  return (
    <button
      onClick={flip}
      className={`btn ${isToggled ? "toggled" : "notToggled"} `}
    >
      {isToggled ? txtToggled : txtNotToggled}
    </button>
  );
};

export default ToggleButton;
