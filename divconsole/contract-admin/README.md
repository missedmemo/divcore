# ✨ Snowpack Client Reference Implementation

    Bootstrapped with create-snowpack-app & react-typescript template,
    with added support for Tailwind CSS and the React Testing Library

<br>

## Scripts Available (after running 'yarn' in root)

### yarn install:all

Runs yarn install on client and webServer projects

### yarn dev

Runs the app in development mode (loads the client in snowpack dev-server).<br>
Open http://localhost:8080 to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### yarn prod

Copies a Webpack-bundled version of your app to a root `dist/` folder.<br>
(to view, navigate to the url specified in the terminal, ex: http://localhost:4000)

### yarn test

Runs tests for all sub-projects

### yarn clean:all

Safely deletes all directories and files listed in .gitignore, except .env

<br>

***

***Why Snowpack?***

> Snowpack reserves bundling (using Webpack) for PRODUCTION builds. Webpack rebundles node_modules etc., on every code change. Snowpack compiles node_modules once, into plain ESM modules, when the project first runs. Since React etc. dependencies don't change as you change code, Snowpack compiles just your code changes on every save, and build times remain at ~50ms regardless of project size. With Webpack, builds slow down as a project grows, and requires instrumenting builds and various analysis & optimization techniques to resolve.

***What about Eject?***

> No eject needed! Snowpack guarantees zero lock-in.

***Still TO DO:***

> Integrate components and themes/styles exported by the Style Guide, with local styles, components and data in a realistic way.