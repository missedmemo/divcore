const express = require('express')
const { resolve } = require('path')
const { PORT, DIST_FOLDER } = process.env
const webServer = express()

webServer.use( express.static(DIST_FOLDER))

webServer.listen( PORT, () => {
  const build = webServer.get('env')
  const shortPath = resolve(DIST_FOLDER).slice(-20)

  console.log(`
    webServer (${build} build)
      listening on port ${PORT}
      serving files from '...${shortPath}'
  `)
})