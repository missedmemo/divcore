import React from "react";
import { render, screen } from "@testing-library/react";

import App from "../app";

test("should display specified page header", () => {
  render(<App caption="Yowza!" />);
  expect(screen.getByText(/yowza/i)).toBeInTheDocument();
});

test("should display default page header", () => {
  render(<App />);
  expect(screen.getByText(/counter/i)).toBeInTheDocument();
});
