import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import Counter from '../counter'

let count: HTMLElement,
    btnIncrement: HTMLElement,
    btnDecrement: HTMLElement,
    btnReset: HTMLElement;

const setup = (Component: JSX.Element) => {
  render(Component);
  count = screen.getByTestId(/count/i);
  btnIncrement = screen.getByRole("button", { name: "+" });
  btnDecrement = screen.getByRole("button", { name: "-" });
  btnReset = screen.getByRole("button", { name: /reset/i });
};

describe("Counter with DEFAULT values", () => {

  beforeEach(() => {
    setup(<Counter />);
  });

  test("should INITIALIZE to expected defaults", () => {
    expect(count).toHaveTextContent("0");
    expect(btnIncrement).toBeEnabled();
    expect(btnDecrement).toBeDisabled();
    expect(btnReset).toBeDisabled();
  });

  test("should INCREMENT & DECREMENT by default step value", () => {
    userEvent.click(btnIncrement);
    expect(count).toHaveTextContent("1");
    expect(btnDecrement).toBeEnabled();
    expect(btnReset).toBeEnabled();

    userEvent.click(btnIncrement);
    expect(count).toHaveTextContent("2");
    userEvent.click(btnDecrement);
    expect(count).toHaveTextContent("1");

    userEvent.click(btnDecrement);
    expect(count).toHaveTextContent("0");
    expect(btnDecrement).toBeDisabled();
    expect(btnReset).toBeDisabled();
  });

  test("should DISABLE DECREMENTING at zero", () => {
    expect(btnDecrement).toBeDisabled();
    expect(count).toHaveTextContent("0");

    userEvent.click(btnDecrement);
    expect(count).toHaveTextContent("0");
    expect(btnDecrement).toBeDisabled();

    userEvent.click(btnIncrement);
    expect(count).toHaveTextContent("1");
    expect(btnDecrement).toBeEnabled();

    userEvent.click(btnDecrement);
    expect(count).toHaveTextContent("0");
    userEvent.click(btnDecrement);
    expect(count).toHaveTextContent("0");

    expect(btnDecrement).toBeDisabled();
  });

  test("should RESET to default values", () => {
    expect(btnReset).toBeDisabled();
    expect(btnDecrement).toBeDisabled();

    userEvent.click(btnIncrement);
    expect(btnReset).toBeEnabled();
    expect(btnDecrement).toBeEnabled();

    userEvent.click(btnIncrement);
    expect(count).toHaveTextContent("2");

    userEvent.click(btnReset);
    expect(count).toHaveTextContent("0");
    expect(btnReset).toBeDisabled();
    expect(btnDecrement).toBeDisabled();
  });
});

describe("Counter with custom INITIAL value", () => {
  const initialValue = 4;

  beforeEach(() => {
    setup(<Counter initialValue={initialValue} />);
  });

  test("should INITIALIZE to expected values", () => {
    expect(count).toHaveTextContent(initialValue.toString());
    expect(btnDecrement).toBeEnabled();
    expect(btnReset).toBeDisabled();
  });

  test("should RESET to specified initial value", () => {
    expect(btnReset).toBeDisabled();

    userEvent.click(btnIncrement);
    expect(btnReset).toBeEnabled();

    userEvent.click(btnIncrement);
    expect(count).toHaveTextContent((initialValue + 2).toString());

    userEvent.click(btnReset);
    expect(count).toHaveTextContent(initialValue.toString());
    expect(btnReset).toBeDisabled();
  });

  test("should decrement PAST initial value, stopping at zero", () => {
    expect(btnDecrement).toBeEnabled();
    expect(btnReset).toBeDisabled();

    userEvent.click(btnDecrement);
    expect(count).toHaveTextContent((initialValue - 1).toString());
    expect(btnReset).toBeEnabled();

    userEvent.click(btnDecrement);
    userEvent.click(btnDecrement);
    userEvent.click(btnDecrement);
    userEvent.click(btnDecrement);
    userEvent.click(btnDecrement);

    expect(count).toHaveTextContent("0");
    expect(btnReset).toBeEnabled();
    expect(btnDecrement).toBeDisabled();
  });
});

describe("Counter with custom STEP value", () => {
  const initialValue = 2;
  const stepValue = 3;

  beforeEach(() => {
    setup(<Counter initialValue={initialValue} stepValue={stepValue} />);
  });

  test("should increment & decrement by specified step value", () => {
    userEvent.click(btnIncrement);
    expect(count).toHaveTextContent((initialValue + stepValue).toString());
    expect(btnDecrement).toBeEnabled();
    expect(btnReset).toBeEnabled();

    userEvent.click(btnIncrement);
    expect(count).toHaveTextContent(
      (initialValue + 2 * stepValue).toString()
    );
    userEvent.click(btnDecrement);
    expect(count).toHaveTextContent((initialValue + stepValue).toString());

    userEvent.click(btnDecrement);
    expect(count).toHaveTextContent(initialValue.toString());
  });

  test("should decrement PAST initial value, STOPPING AT ZERO", () => {
    expect(btnDecrement).toBeEnabled();
    expect(btnReset).toBeDisabled();

    userEvent.click(btnDecrement);
    expect(count).toHaveTextContent("0");

    expect(btnDecrement).toBeDisabled();
    expect(btnReset).toBeEnabled();
  });
});