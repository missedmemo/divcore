import React from 'react';
import Counter from './counter'
import Stopwatch from './stopwatch'

interface IApp {
  caption?: string;
}

const App = ({ caption = "A Simple Counter..." }: IApp): JSX.Element => (
  <div className="h-full flex flex-col items-center justify-center text-4xl">
    <header className='mb-8'>{caption}</header>
    <Counter />
    <Stopwatch />
  </div>
);


export default App;
