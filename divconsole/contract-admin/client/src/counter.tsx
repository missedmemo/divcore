import React, { useState } from 'react';

//import "./counter.css";

interface ICounterProps {
  initialValue?: number;
  stepValue?: number;
}

const Counter = ({
  initialValue = 0,
  stepValue = 1,
}: ICounterProps): JSX.Element => {
  const [count, setCount] = useState(initialValue);

  const increment = () => {
    setCount((c) => c + stepValue);
  };

  const decrement = () => {
    if (count < stepValue) {
      if (count > 0) {
        setCount(0);
      }
    } else {
      setCount((c) => c - stepValue);
    }
  };

  const reset = () => {
    setCount(initialValue);
  };

  return (
    <div className="frame">
      <div className="flex items-center">
        <button onClick={increment} className='btn-green mr-2'>+</button>
        Count is:{" "}
        <span className="text-6xl mx-2" data-testid="count">
          {count}
        </span>
        <button onClick={decrement} className="btn-green disablable" disabled={count === 0}>
          -
        </button>
      </div>

      <button
        className="btn-reset w-full mt-1 disablable"
        onClick={reset}
        disabled={count === initialValue}
      >
        RESET
      </button>
    </div>
  );
};

export default Counter;

