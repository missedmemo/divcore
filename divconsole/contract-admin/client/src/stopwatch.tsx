import React, { useState } from 'react';
// @ts-ignore
import { ToggleButton } from '../../../../Divisions.StyleGuide/dist';

/*******************************************************************
 Once the component lib reference above is working,
 alias the relative path in snowpack.config, as below:

  resolve: {
    alias: {
      '@Components': path.resolve('../../Divisions.StyleGuide/dist')
    }
  }
********************************************************************/

const Stopwatch = (): JSX.Element => {

  return (
    <div>
      <ToggleButton />
    </div>
  );
};

export default Stopwatch;

